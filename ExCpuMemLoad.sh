source /etc/profile

DataOfStatCpuMem=`date --date '-1 day' +%Y%m%d`

cpuMailFile="CPULoad_Name_Data_Cnt_OverCnt_Ratio_$DataOfStatCpuMem.csv"
memMailFile="MemLoad_Name_Data_Cnt_OverCnt_Ratio_$DataOfStatCpuMem.csv"

./CpuMemLoad

mail_list="gaozhongsheng@letv.com,gaoxw126@126.com"
cpu_mail_title="CPU Over Load Statistic $DataOfStatCpuMem"
mem_mail_title="Mem Over Load Statistic $DataOfStatCpuMem"

python ../sendmail.py $cpuMailFile $mail_list $cpu_mail_title
python ../sendmail.py $memMailFile $mail_list $mem_mail_title

rm $cpuMailFile
rm $memMailFile

echo "SendMail Successful. "

